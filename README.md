# Partial r/unixporn 2018 survey results

This are some results of the 2018 survey made in the subreddit [r/unixporn](https://www.reddit.com/r/unixporn/). 
The results are partial because some answers made by the users, need some filtering/correction. Also because some
extra commentary is missing, like in [previous](https://imgur.com/a/0USMR) surveys.

For now, only a couple of the results for linux are posted, and the charts 
need some retouching (for example, add the percentage of an answer in a chart or add some visual queues for a better understanding of the data).
The results for BSD, macOS, Windows and Mobile are already computed but I need some time to export the charts.

The data was donwloaded from [here](https://github.com/unixporn/surveys).

# General

## What is your gender?
The category 'other' includes people who didn't answer.

![gender](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=2107913375&format=image)

## How old are you?

![age](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=35573614&format=image)

## Where is home?
The first map shows results including all countries.

![country1](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=801571651&format=image)

Here's an interactive version of the map, so you can see how many people are in each country.

![country2](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=623392522&format=interactive)

## What is your occupation?
This one was a little bit tricky, since there were several answers per person. However, the results are similar to the ones in 2017.

![job](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=878507325&format=image)

## How do you interact with /r/unixporn?
I need to check the question for this one, because the answers make no sense.

![activity](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=1595555191&format=image)

## How happy are you with the sub?
Rating of the sub with 5 being very happy and 1 being not happy at all.

![rating](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=1656663043&format=image)

# Linux
## Do you use Linux?
![linux](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=1098282130&format=image)

## What distro do you use?
The category 'other' includes missing answers as well.

![distro](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=1512048115&format=image)

## Why that distro?
The answers in 'other' where very varied, so I clumped them all together. 

![why_distro](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=304387861&format=image)

## What DE/WM do you use?
![linux_de_wm](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=772213344&format=image)

## Why that DE/WM?
![linux_why_de_wm](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=1312355813&format=image)

## What browser do you use?
![linux_broser](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=341135497&format=image)

## What shell do you use?
![linux_shell](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=827969959&format=image)

## What editor do you use?
![linux_editor](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=249305894&format=image)

## What terminal emulator do you use?
![linux_term](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=834906183&format=image)

## What do you use it on?
![linux_hardware](https://docs.google.com/spreadsheets/d/e/2PACX-1vRGEpyLw3Ebxh9HiFI1_gLK5Acqet1AfeUfXtxgtbSKai_ow8N6Lcj2yyDcLdWLis4sUU3QD9zeyZ_w/pubchart?oid=1002893173&format=image)

# BSD

# macOS

# Windows

# Mobile